<?php
use App\Http\Controllers\ProductController;
$total=0;
if(Session::has('user'))
{
  $total = ProductController::cartItem();
}


?>


<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="/">Basic Laravel</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="/cartlist">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/myorders">Orders</a>
          </li>
        </ul>
        <form class="d-flex">
          <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" style="width: 150px; margin-top:6px; margin-left:60px;">
          <button class="btn btn-success" type="submit" style="margin-top:6px;">Search</button>
        </form>
        <ul class="nav navbar-nav navbar-right">
          <li style="margin-top: -50px;"><a href="/cartlist" style="color: #fff; background:#343a40!important">Card({{$total}})</a></li>
          @if(Session::has('user'))
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="margin-top: -50px; color:#fff; background:#343a40!important">{{Session::get('user')['name']}}
              <span class="caret" style="display: none;"></span></a>
              <ul class="dropdown-menu">
                <li><a href="/logout">Logout</a></li>
              </ul>
            </li>
            @else
            <li><a href="/login" style="margin-top: -50px; color:#fff; background:#343a40!important">Login</a></li>
            <li><a href="/register" style="margin-top: -50px; color:#fff; background:#343a40!important">Register</a></li>
          @endif
        </ul>
      </div>
    </div>
</nav>