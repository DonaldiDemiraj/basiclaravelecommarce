@extends('master')
@section('contents')
    <div class="custom-product">
        {{-- <div class="col-sm-4">
            <a href="#">Filter</a>
        </div> --}}
        <div class="col-sm-10">
            <table class="table">
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Amount</td>
                        <td>€ {{$total}}</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Tax</td>
                        <td>€ 0</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Delivery</td>
                        <td>€ 10</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Total Amount</td>
                        <td>€ {{$total + 10}}</td>
                    </tr>
                </tbody>
            </table>
            <div>
                <form action="/orderplace" method="POST">
                    @csrf
                    <div class="form-group">
                      <textarea name="addres" placeholder="Enter your addres..." class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                      <label>Payment Method:</label><br>
                      <input type="radio" value="cash" name="payment">
                      <span>Online payment</span><br>
                      <input type="radio" value="cash" name="payment">
                      <span>EMI payment</span><br>
                      <input type="radio" value="cash" name="payment">
                      <span>Payment on Delivery</span>
                    </div>
                    <button type="submit" class="btn btn-default">Order Now</button>
                </form>
            </div>
        </div>
    </div>
@endsection