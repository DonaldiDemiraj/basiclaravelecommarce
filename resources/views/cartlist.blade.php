@extends('master')
@section('contents')
    <div class="custom-product">
        {{-- <div class="col-sm-4">
            <a href="#">Filter</a>
        </div> --}}
        <div class="col-sm-10">
            <div class="trending-wrapperr">
                <h4>Result for Products</h4>
                <a class="btn btn-success" href="ordernow">Order Now</a><br><br>
                @foreach ($products as $item)
                    <div class="row searched-item cart-list-devider">
                        <div class="col-sm-4">
                            <a href="detail/{{$item->id}}"></a>
                            <img class="trending-image" src="{{$item->gallery}}" alt="">
                        </div>

                        <div class="col-sm-4">
                            <a href="detail/{{$item->id}}"></a>
                            <div class="">
                                <h2>{{$item->name}}</h2>
                                <h5>{{$item->description}}</h5>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <a href="/removecart/{{$item->cart_id}}" class="btn btn-warning">Remove to Cart</a>
                        </div>
                    </div>
                @endforeach
            </div>
            <a class="btn btn-success" href="ordernow">Order Now</a><br><br>
        </div>
    </div>
@endsection