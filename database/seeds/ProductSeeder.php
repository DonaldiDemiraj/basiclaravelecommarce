<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert(
        //     [
        //     'name' => 'Samsung mobile',
        //     "price" => "200 €",
        //     "description" => "A smartphone with 12GB ram,64 MPX camera and 5000 battery",
        //     "category" => "mobile",
        //     "gallery" => "https://cdn.mos.cms.futurecdn.net/kk5o5PsWe5D9ZKWijika7m.jpg"    
        // ]

        // [
        //     'name' => 'Samsung Tv',
        //     "price" => "900 €",
        //     "description" => "Tv led with 42 inc ecran,4D ultra HD",
        //     "category" => "TV",
        //     "gallery" => "https://images.samsung.com/is/image/samsung/latin-en-uhd-tu8000-un50tu8000pxpa-frontblack-286008779?$720_576_PNG$"    
        // ]

        // [
        //     'name' => 'Sony Tv',
        //     "price" => "600 €",
        //     "description" => "Tv led with 32 inc ecran, HD",
        //     "category" => "TV",
        //     "gallery" => "https://www.sony-mea.com/image/2a8264bd4060bb485e1deeeec1c5564a?fmt=png-alpha&resMode=bisharp&wid=384"    
        // ]

        [
            'name' => 'LG fridge',
            "price" => "700 €",
            "description" => "A fridge with much more features",
            "category" => "fridge",
            "gallery" => "https://www.antaki.com.lb/wp-content/uploads/2020/11/Sample-Frame-8.jpg"    
        ]
    
    );
    }
}
